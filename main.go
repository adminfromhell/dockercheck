package main

import (
	"bytes"
	"github.com/pkg/errors"
	"gopkg.in/alecthomas/kingpin.v2"
	"io/ioutil"
	"net/http"
	"encoding/json"
	"os"
	"strings"
)

var (
	useJSON = kingpin.Flag("json", "read json response from target.").Short('j').Bool()
	useText = kingpin.Flag("text", "read text response from target.").Short('t').Bool()
	target = kingpin.Arg("target", "target to check response from .").Required().String()
	usePOST = kingpin.Flag("post", "make a post request.").Bool()
	useGET = kingpin.Flag("get", "make a get request.").Bool()
)

const (
	HEALTHY = 0
	UNHEALTHY = 1
)

func responseType() (string, error) {
	if *useJSON {
		return "application/json", nil
	}
	if *useText {
		return "text/plain", nil
	}
	return "", errors.New("no response type specified.")
}

func makePOSTRequest() (res map[string]interface{}, err error) {
	message := map[string]string{
		"type": "healthcheck",
	}
	marshaledMessage, err := json.Marshal(message)
	if err != nil {
		return nil, err
	}
	respType, err := responseType()
	if err != nil {
		return nil, err
	}
	resp, err := http.Post(*target, respType, bytes.NewBuffer(marshaledMessage))
	if err != nil {
		return nil, err
	}
	var result map[string]interface{}

	err = json.NewDecoder(resp.Body).Decode(&result)
	if err != nil {
		return nil, err
	}
	return result, nil
}

func makeGETRequest() (res int, err error) {
	resp, err := http.Get(*target)
	if err != nil {
		return UNHEALTHY, err
	}

	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return UNHEALTHY, err
	}
	if strings.ToLower(string(body)) == "healthy" {
		return HEALTHY, nil
	} else if strings.ToLower(string(body)) == "true" {
		return HEALTHY, nil
	} else {
		return UNHEALTHY, nil
	}
}

func checkJSONResp(res map[string]interface{}) int {
	if val, ok := res["status"]; ok {
		if strings.ToLower(val.(string)) == "healthy" {
			return HEALTHY
		} else {
			return UNHEALTHY
		}
	}
	if val, ok := res["healthy"]; ok {
		if val.(bool) {
			return HEALTHY
		} else {
			return UNHEALTHY
		}
	}
	return UNHEALTHY
}

func makeRequest() (exitCode int) {
	if *usePOST {
		resp, err := makePOSTRequest()
		if err != nil {
			return UNHEALTHY
		}
		return checkJSONResp(resp)
	} else if *useGET {
		resp, err := makeGETRequest()
		if err != nil {
			return UNHEALTHY
		}
		return resp
	} else {
		return UNHEALTHY
	}
}

func main() {
	kingpin.Parse()
	status := makeRequest()
	os.Exit(status)
}